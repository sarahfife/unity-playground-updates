﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Playground/Actions/Animation")]
public class AudioAction : Action
{
    public enum AudioType
    {
        SOUND_EFFECT,
        MUSIC
    }

    public AudioClip soundFile;

    public AudioType audioType = AudioType.SOUND_EFFECT;

    [ShowOnEnum("audioType", (int)AudioType.MUSIC)]
    public MusicPlayer musicPlayer;


    public override bool ExecuteAction(GameObject dataObject)
    {
        if (soundFile != null)
        {

            switch(audioType)
            {
                case AudioType.SOUND_EFFECT:
                    AudioSource.PlayClipAtPoint(soundFile, transform.position);
                    return true;

                case AudioType.MUSIC:
                    if (musicPlayer != null)
                    {
                        musicPlayer.PlayMusic(soundFile);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
